import { writable, get } from 'svelte/store';
import { toast } from '@zerodevx/svelte-toast';

const backend = writable(null);

export async function init_backend() {
	// Already init
	if (get(backend) !== null || !window.location.href.includes('localhost')) {
		return;
	}

	// Ask for the backend
	let backend_address = localStorage.getItem('backend_address');
	if (backend_address === null) {
		backend_address = prompt('Please enter the backend server address');
		if (backend_address === null) {
			return;
		}
	}

	localStorage.setItem('backend_address', backend_address);
	backend.set(backend_address);
}

export function get_server() {
	let current = window.location.origin;
	if (current.includes('localhost')) return 'http://localhost:8000';
	else return current;
}

export async function call_backend(
	method,
	path,
	body,
	error_message,
	no_answer = false,
	is_json = true
) {
	if (body !== null && is_json) {
		body = JSON.stringify(body);
	}
	const response = await fetch(get_server() + path, {
		method: method,
		body: body
	});
	if (!response.ok) {
		let text = await response.text();
		toast.push(`${error_message}: ${text}`);
		return null;
	}
	if (no_answer) {
		return true;
	}
	if (is_json) {
		return await response.json();
	} else {
		return await response.text();
	}
}

export async function get_plants() {
	return await call_backend('GET', '/plant', null, 'Failed to fetch categories');
}
