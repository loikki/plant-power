// Prevents additional console window on Windows in release, DO NOT REMOVE!!
#![cfg_attr(
    all(not(debug_assertions), target_os = "windows"),
    windows_subsystem = "windows"
)]

// With tauri
#[cfg(feature = "tauri")]
use plant_power_lib::start_with_tauri;

#[cfg(feature = "tauri")]
pub fn main() {
    start_with_tauri();
}

// Without tauri
#[cfg(feature = "diesel")]
use rocket::{fs::FileServer, launch};

#[cfg(feature = "diesel")]
use plant_power_lib::backend;

#[cfg(feature = "diesel")]
#[launch]
fn rocket() -> _ {
    use plant_power_lib::config::Config;
    let config = Config::new();
    backend::get_rocket().mount("/", FileServer::from(config.frontend_path()))
}
