// @generated automatically by Diesel CLI.

diesel::table! {
    plant (id) {
        id -> Unsigned<Bigint>,
        #[max_length = 255]
        name -> Varchar,
        freq_days -> Unsigned<Integer>,
        last_watering -> Datetime,
        sowed -> Datetime,
        #[max_length = 255]
        image_name -> Nullable<Varchar>,
    }
}
