use rocket::serde::json::Json;
use rocket::tokio::sync::Mutex;
use rocket::{delete, get, put, routes, Route, State};

use crate::config::Config;
#[cfg(feature = "diesel")]
use crate::database::Database;
use crate::plant::Plant;
use crate::plant_error::PlantError;

pub fn get_routes() -> Vec<Route> {
    routes![get_plant, get_plants, add_plant, delete_plant,]
}

#[get("/<plant_id>")]
pub async fn get_plant(
    plant_id: u64,
    config: &State<Mutex<Config>>,
) -> Result<Json<Plant>, PlantError> {
    let config = config.inner().lock().await;
    #[cfg(feature = "diesel")]
    {
        let mut db = Database::new(config.database_url());
        let plant = db.get_plant_by_id(plant_id)?;
        return Ok(Json(plant));
    }
    Err(PlantError::Plant("Not implemented".to_string()))
}

#[get("/")]
pub async fn get_plants(config: &State<Mutex<Config>>) -> Result<Json<Vec<Plant>>, PlantError> {
    let config = config.inner().lock().await;
    #[cfg(feature = "diesel")]
    {
        let mut db = Database::new(config.database_url());
        let plants = db.get_plants();
        return Ok(Json(plants));
    }
    Err(PlantError::Plant("Not implemented".to_string()))
}

#[put("/", data = "<plant>")]
pub async fn add_plant(
    plant: Json<Plant>,
    config: &State<Mutex<Config>>,
) -> Result<Json<u64>, PlantError> {
    let config = config.inner().lock().await;
    #[cfg(feature = "diesel")]
    {
        let mut db = Database::new(config.database_url());
        let plant = plant.into_inner();
        let id = db.add_or_update_plant(&plant)?;
        return Ok(Json(id));
    }
    Err(PlantError::Plant("Not implemented".to_string()))
}

#[delete("/<plant_id>")]
pub async fn delete_plant(plant_id: u64, config: &State<Mutex<Config>>) -> Result<(), PlantError> {
    let config = config.inner().lock().await;
    #[cfg(feature = "diesel")]
    {
        let mut db = Database::new(config.database_url());
        db.delete_plant_by_id(plant_id)?;
        return Ok(());
    }
    Err(PlantError::Plant("Not implemented".to_string()))
}
