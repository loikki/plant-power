use rocket::fs::TempFile;
use rocket::serde::json::Json;
use rocket::tokio::sync::Mutex;
use rocket::{get, post, put, routes, Route, State};
use uuid::Uuid;

use crate::config::Config;
#[cfg(feature = "diesel")]
use crate::database::Database;
use crate::plant::Plant;
use crate::plant_error::PlantError;

pub fn get_routes() -> Vec<Route> {
    routes![sow, water, get_watering, add_image,]
}

#[post("/sow/<plant_id>")]
pub async fn sow(plant_id: u64, config: &State<Mutex<Config>>) -> Result<(), PlantError> {
    let config = config.inner().lock().await;
    #[cfg(feature = "diesel")]
    {
        let mut db = Database::new(config.database_url());
        return db.sow_plant(plant_id);
    }
    Err(PlantError::Plant("Not implemented".to_string()))
}

#[post("/water/<plant_id>")]
pub async fn water(plant_id: u64, config: &State<Mutex<Config>>) -> Result<(), PlantError> {
    let config = config.inner().lock().await;
    #[cfg(feature = "diesel")]
    {
        let mut db = Database::new(config.database_url());
        return db.water_plant(plant_id);
    }
    Err(PlantError::Plant("Not implemented".to_string()))
}

#[get("/watering")]
pub async fn get_watering(config: &State<Mutex<Config>>) -> Result<Json<Vec<Plant>>, PlantError> {
    let config = config.inner().lock().await;
    #[cfg(feature = "diesel")]
    {
        let mut db = Database::new(config.database_url());
        let plants = db.get_watering()?;
        return Ok(Json(plants));
    }
    Err(PlantError::Plant("Not implemented".to_string()))
}

#[put("/image", data = "<file>")]
pub async fn add_image(
    mut file: TempFile<'_>,
    config: &State<Mutex<Config>>,
) -> Result<String, PlantError> {
    let config = config.inner().lock().await;
    #[cfg(feature = "diesel")]
    {
        let uuid = Uuid::new_v4();
        let filename = format!("{}/{}", config.image_path(), uuid);
        file.copy_to(filename).await?;
        return Ok(uuid.to_string());
    }
    Err(PlantError::Plant("Not implemented".to_string()))
}
