pub mod backend;
pub mod config;
pub mod plant;
pub mod plant_error;
mod routes;

#[cfg(feature = "diesel")]
pub mod database;
#[cfg(feature = "diesel")]
pub mod schema;

// android
#[cfg(target_os = "android")]
use tauri;

// tauri
#[cfg(any(feature = "tauri", target_os = "android"))]
pub mod gui;

#[cfg(any(feature = "tauri", target_os = "android"))]
use rocket::{info, tokio};

#[cfg(any(feature = "tauri", target_os = "android"))]
use crate::gui::AppBuilder;

#[cfg(any(feature = "tauri", target_os = "android"))]
pub fn start_with_tauri() {
    info!("Starting app");

    // Start rocket
    let rt = tokio::runtime::Runtime::new().unwrap();
    rt.spawn(async { backend::get_rocket().launch().await });

    // Start tauri
    AppBuilder::new().run();
}

#[cfg(target_os = "android")]
#[tauri::mobile_entry_point]
fn android_start() {
    start_with_tauri();
}
