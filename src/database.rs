use chrono::{Days, Local, NaiveDateTime};
use diesel::mysql::MysqlConnection;
use diesel::prelude::Insertable;
use diesel::Connection;
use diesel::ExpressionMethods;
use diesel::OptionalExtension;
use diesel::{QueryDsl, RunQueryDsl, SelectableHelper};
use diesel_migrations::{embed_migrations, EmbeddedMigrations, MigrationHarness};

use crate::plant::Plant;
use crate::plant_error::PlantError;
use crate::schema::plant;

pub struct Database {
    sql: MysqlConnection,
}

fn get_next_watering(plant: &Plant) -> NaiveDateTime {
    plant.last_watering.unwrap() + Days::new(plant.freq_days.into())
}

impl Database {
    pub fn new(url: &String) -> Self {
        Self {
            sql: MysqlConnection::establish(url).unwrap(),
        }
    }

    pub fn migrate(&mut self) {
        // Migrate DB
        const MIGRATIONS: EmbeddedMigrations = embed_migrations!("migrations/");
        self.sql
            .run_pending_migrations(MIGRATIONS)
            .expect("Failed to run migration");
    }

    pub fn get_plants(&mut self) -> Vec<Plant> {
        let mut plants = plant::table
            .select(Plant::as_select())
            .load(&mut self.sql)
            .expect("Failed to fetch plant");
        plants.sort_by(|a, b| {
            get_next_watering(a)
                .partial_cmp(&get_next_watering(b))
                .unwrap()
        });
        plants
    }

    pub fn add_or_update_plant(&mut self, new_plant: &Plant) -> Result<u64, PlantError> {
        if new_plant.id.is_some() {
            diesel::update(plant::table)
                .filter(plant::id.eq(new_plant.id.unwrap()))
                .set(new_plant)
                .execute(&mut self.sql)?;
            Ok(new_plant.id.unwrap())
        } else {
            // Check if we already have this plant
            let plant = self.get_plant_by_name(&new_plant.name)?;
            if plant.is_some() {
                let mut new_plant = (*new_plant).clone();
                new_plant.id = plant.unwrap().id;
                return self.add_or_update_plant(&new_plant);
            }

            // We truly don't know this plant => create it
            diesel::insert_into(plant::table)
                .values(new_plant)
                .execute(&mut self.sql)?;
            let plant = self.get_plant_by_name(&new_plant.name)?;
            Ok(plant.unwrap().id.unwrap())
        }
    }

    pub fn get_plant_by_id(&mut self, id: u64) -> Result<Plant, PlantError> {
        let plant = plant::table
            .select(Plant::as_select())
            .filter(plant::id.eq(id))
            .first(&mut self.sql)?;
        Ok(plant)
    }

    pub fn delete_plant_by_id(&mut self, id: u64) -> Result<(), PlantError> {
        diesel::delete(plant::table.filter(plant::id.eq(id))).execute(&mut self.sql)?;
        Ok(())
    }

    pub fn get_plant_by_name(&mut self, name: &String) -> Result<Option<Plant>, PlantError> {
        let plant = plant::table
            .select(Plant::as_select())
            .filter(plant::name.eq(name))
            .first(&mut self.sql)
            .optional()?;
        Ok(plant)
    }

    pub fn sow_plant(&mut self, id: u64) -> Result<(), PlantError> {
        diesel::update(plant::table)
            .filter(plant::id.eq(id))
            .set(plant::sowed.eq(Local::now().naive_local()))
            .execute(&mut self.sql)?;
        Ok(())
    }

    pub fn water_plant(&mut self, id: u64) -> Result<(), PlantError> {
        diesel::update(plant::table)
            .filter(plant::id.eq(id))
            .set(plant::last_watering.eq(Local::now().naive_local()))
            .execute(&mut self.sql)?;
        Ok(())
    }

    pub fn get_watering(&mut self) -> Result<Vec<Plant>, PlantError> {
        // TODO single query
        let mut plants = plant::table
            .select(Plant::as_select())
            .get_results(&mut self.sql)?;

        let now = Local::now().naive_local();
        plants.retain(|x| {
            let watering = x.last_watering.unwrap() + Days::new(x.freq_days.into());
            watering < now
        });
        Ok(plants)
    }
}
