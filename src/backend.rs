use rocket::fs::FileServer;
use rocket::tokio::sync::Mutex;
use rocket::{http::Method, Build, Rocket};
use rocket_cors::{AllowedOrigins, CorsOptions};
use std::net::Ipv4Addr;

use crate::routes::action;
use crate::routes::plant;

#[cfg(feature = "diesel")]
use futures::executor::block_on;

use crate::config::Config;

pub fn get_rocket() -> Rocket<Build> {
    // Config
    let config = Config::new();
    let image_path = config.image_path().clone();
    let ip = if config.expose_rocket() {
        Ipv4Addr::new(0, 0, 0, 0)
    } else {
        Ipv4Addr::new(127, 0, 0, 1)
    };
    let config: Mutex<Config> = Mutex::new(config);

    let rocket_config = rocket::Config::figment().merge(("address", ip));

    // Migrate DB
    #[cfg(feature = "diesel")]
    {
        let config = block_on(config.lock());
        if config.backend_url().is_none() {
            use crate::database::Database;
            Database::new(config.database_url()).migrate();
        }
    }

    // Cors
    let cors = CorsOptions::default()
        .allowed_origins(AllowedOrigins::all())
        .allowed_methods(
            vec![
                Method::Get,
                Method::Post,
                Method::Put,
                Method::Delete,
                Method::Patch,
            ]
            .into_iter()
            .map(From::from)
            .collect(),
        )
        .allow_credentials(true);

    rocket::custom(rocket_config)
        .manage(config)
        .mount("/", action::get_routes())
        .mount("/plant", plant::get_routes())
        .mount("/image", FileServer::from(image_path).rank(9))
        .attach(cors.to_cors().unwrap())
}
