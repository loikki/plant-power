use chrono::NaiveDateTime;
#[cfg(feature = "diesel")]
use diesel::{AsChangeset, Insertable, Queryable, Selectable};
use serde::{Deserialize, Serialize};

#[cfg_attr(
    feature = "diesel",
    derive(Selectable, Queryable, AsChangeset, Insertable)
)]
#[cfg_attr(feature="diesel", diesel(table_name=crate::schema::plant))]
#[derive(Deserialize, Serialize, Clone)]
pub struct Plant {
    #[cfg_attr(feature="diesel", diesel(deserialize_as = u64))]
    pub id: Option<u64>,
    pub name: String,
    pub freq_days: u32,
    #[cfg_attr(feature="diesel", diesel(deserialize_as = NaiveDateTime))]
    pub last_watering: Option<NaiveDateTime>,
    #[cfg_attr(feature="diesel", diesel(deserialize_as = NaiveDateTime))]
    pub sowed: Option<NaiveDateTime>,
    pub image_name: Option<String>,
}
