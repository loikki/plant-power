CREATE TABLE plant (
  id SERIAL PRIMARY KEY,
  name varchar(255) NOT NULL,
  UNIQUE(name),
  freq_days INT UNSIGNED NOT NULL,
  last_watering DATETIME NOT NULL DEFAULT NOW(),
  sowed DATETIME NOT NULL DEFAULT NOW(),
  image_name varchar(255) DEFAULT NULL
);

CREATE INDEX idx_name ON plant(name);
